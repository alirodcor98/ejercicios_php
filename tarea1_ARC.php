<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio estructuras de control PHP</title>
</head>
<body>
    
    <?php 

    // Nombre: Alí Rodríguez

    echo "Imprimiendo triangulo";
    echo "<br><br>";

    for($i = 1; $i <= 30; $i++){
        echo "<center>";
        for($j = 1; $j <= $i; $j++){
            echo "*";
        }
        echo "</center>";
        //echo "<br>";
    }

    echo "<br><br>";
    echo "Imprimiendo rombo";
    echo "<br><br>";
    for($i = 1; $i <= 30; $i++){
        echo "<center>";
        for($j = 1; $j <= $i; $j++){
            echo "*";
        }
        echo "</center>";
        //echo "<br>";
    }
    for($i = 29; $i >= 1; $i--){
        echo "<center>";
        for($j = 1; $j <= $i; $j++){
            echo "*";
        }
        echo "</center>";
        //echo "<br>";
    }

    ?>

</body>
</html>