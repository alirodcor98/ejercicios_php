<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro alumnos</title>
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet"
          href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
    <style>
        #menu-opciones{
            background-color: blue;
            height: 40px;
        }

        #opcion-home{
            color: #FFFFFF;
            padding: 2px;
        }

        #opcion-registro{
            color: #FFFFFF;
            padding: 2px;
        }

        #opcion-exit{
            color: #FFFFFF;
            padding: 2px;
        }

        #tabla-datos-guardados{
            border-collapse: separate;
        }
    </style>
</head>
<body>

    <nav id="menu-opciones">
        <a href="info.php" id="opcion-home">Home</a>
        <a href="formulario.php" id="opcion-registro">Registrar alumnos</a>
        <a href="login.php" id="opcion-exit">Cerrar sesión</a>
    </nav>

    <h2>Formulario de registro</h2>
    <br>
        <div class="container">
        <div class= "columns">
        <form action="formulario.php" method="POST" enctype="multipart/form-data">

            <label class="form-label" for="input-text">Número de cuenta:</label>
                <input name="No_cuenta" class="form-input " type="text" id="input-noCuenta" placeholder="No. cuenta">
                
            <label class="form-label" for="input-text">Nombre:</label>
                <input name="nombre" class="form-input" type="text" id="input-nombre" placeholder="Nombre">
                <br>
            <label class="form-label" for="input-text">Primer apellido:</label>
                <input name="primer_apellido" class="form-input" type="text" id="input-primerAp" placeholder="Primer Apellido">
                <br>
            
            <label class="form-label" for="input-text">Segundo apellido:</label>
                <input name="segundo_apellido" class="form-input" type="text" id="input-segundoAp" placeholder="Segundo Apellido">
                <br>
            
            <label class="form-label">Género:</label>

            <label class="form-radio">
				<input type="radio" name="genero" value="H" checked>
					<i class="form-icon"></i> Hombre
			</label>
            <br>

			<label class="form-radio">
				<input type="radio" name="genero" value="M">
				    <i class="form-icon"></i> Mujer
			</label>
            <br>
            <label class="form-radio">
            <input type="radio" name="genero" value="O">
            <i class="form-icon"></i> Otro
            </label>
             <br>
            <label class="form-label" for="input-text">Fecha de nacimiento:</label>
            <input name="fecha_Nac" class="form-input" type="text" id="input-fechaNac" placeholder="dd/mm/aaaa">
            <br>
                    
            <label class="form-label" for="input-password">Contraseña:</label>
                <input name="password" class="form-input" type="password" id="input-password" placeholder="Contraseña">
                <input type='submit' class="btn" value="Registrar"/>
                    <input type='reset' class="btn btn-primary" value="Limpiar"/>

    <?php session_start();

        foreach($_SESSION['Alumno'] as $llave => $valor){
            if(!empty($_POST)){
                array_push($_SESSION['Alumno'], array('num_cta' => $_POST['No_cuenta'],
                'nombre' => $_POST['nombre'],
                'primer_apellido' => $_POST['primer_apellido'],
                'segundo_apellido' => $_POST['segundo_apellido'],
                'contrasenia' => $_POST['password'],
                'genero' => $_POST['genero'],
                'fecha_nac' => $_POST['fecha_Nac']
                )
            );
            }
        }
        
    ?>
    
</body>
</html>