<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Info</title>
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet"
          href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
    <style>
        #menu-opciones{
            background-color: blue;
            height: 40px;
        }

        #opcion-home{
            color: #FFFFFF;
            padding: 2px;
        }

        #opcion-registro{
            color: #FFFFFF;
            padding: 2px;
        }

        #opcion-exit{
            color: #FFFFFF;
            padding: 2px;
        }

        #tabla-datos-guardados{
            border-collapse: separate;
        }
    </style>
</head>
<body>

    <nav id="menu-opciones">
        <a href="info.php" id="opcion-home">Home</a>
        <a href="formulario.php" id="opcion-registro">Registrar alumnos</a>
        <a href="login.php" id="opcion-exit">Cerrar sesión</a>
    </nav>

    <?php session_start();
    

        echo "<div id = \"datos_loggeado\">";
        echo "<h2 id=\"encabezado-usuario\">Usuario autenticado</h2>";
        echo "<table id=\"tabla-datos-autenticado\" border=1>";
        echo "<tr id=\"Tabla usuario autenticado\">";
        foreach($_SESSION['Alumno'] as $llave => $valor){
                if($valor['num_cta'] == $_SESSION['usuario_loggeado_actual']){
                        echo "<tr>";
                        echo "<td>";
                        echo $valor['nombre']." ".$valor['primer_apellido'];
                        echo "</td>";
                        echo "<td>";
                        echo "<h3>Información</h3>";
                        echo "Número de cuenta: ".$_SESSION['usuario_loggeado_actual'];
                        echo "<br>";
                        echo "Fecha de nacimiento: ".$valor['fecha_nac'];
                        echo "</td>";
                        echo "</tr>";
                }
        }
        echo "</tr>";
        echo "</table>";
        echo "</div>";

        echo "<br>";
        
        echo "<div id= \"datos-usuario\">";
        echo "<h2 id=\"encabezado-datos\">Datos guardados</h2>";
        echo "<table id=\"tabla-datos-guardados\" border=1>";
        echo "<tr id=\"tabla-datos\">";
            echo "<th>#</th>";
            echo "<th>Nombre</th>";
            echo "<th>Fecha de nacimiento</th>";
        echo "</tr>";
            echo "<br>";
            foreach($_SESSION['Alumno'] as $llave => $valor){
                if(!empty($_SESSION)){
                    echo "<tr>";
                    echo "<td>";
                    echo $llave;
                    echo "</td>";
                    echo "<td>";
                    echo $valor['nombre'];
                    echo "</td>";
                    echo "<td>";
                    echo $valor['fecha_nac'];
                    echo "</td>";
                    echo "</tr>";
                }
            }
        echo "</table>";
    echo "</div>";
    
    ?>
</body>
</html>