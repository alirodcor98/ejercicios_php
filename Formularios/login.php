<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet"
          href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
</head>
<body>

    <div class = "container">
        <div class = "columns">
            <form action="login.php" method="POST" enctype="multipart/form-data">
                <label class="form-label" for="input-integer">Número de cuenta: </label>
                <input name="cuenta" class="form-input" id="input-nocuenta" type="text" placeholder="No. de cuenta">
                <br>
                <label class="form-label" for="input-passwd">Contraseña: </label>
                <input name="contrasena" class="form-input" id="input-passwd" type="password" placeholder="Contraseña">
                <br>
                <input type='submit' class="btn" value="Entrar"/>
            </form>
        </div>
    </div>
    
    <?php session_start();


            $_SESSION['Alumno'] = [
                1 => [
                    'num_cta' => '1',
                    'nombre' => 'Admin',
                    'primer_apellido' => 'General',
                    'segundo_apellido' => '',
                    'contrasenia' => 'adminpass123',
                    'genero' => 'O',
                    'fecha_nac' => '25/01/1990'
                ],

                2 => [
                    'num_cta' => '2',
                    'nombre' => 'Alí',
                    'primer_apellido' => 'Rodríguez',
                    'segundo_apellido' => '',
                    'contrasenia' => '123',
                    'genero' => 'H',
                    'fecha_nac' => '11/05/1998'
                ],

            ];

            $bandera = 0;
            $_SESSION['usuario_loggeado_actual'] = "";

            foreach($_SESSION['Alumno'] as $llave => $valor){
                if(!empty($_POST)){
                    if($valor['num_cta'] == $_POST['cuenta']){
                        $bandera++;
                        $_SESSION['usuario_loggeado_actual']  = $valor['num_cta'];
                        if($valor['contrasenia'] == $_POST['contrasena']){
                            $bandera++;
                        }
                    }
                }
            }
     
                
            if($bandera == 2){
                header('Location: info.php');
            }     
	    
    ?>
</body>
</html>